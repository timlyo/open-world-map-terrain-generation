use open_world_map::World;
use noise::{OpenSimplex, NoiseFn};
use itertools::iproduct;
use open_world_map::world::DataPoint;
use noise::Seedable;
use rand::random;

pub fn generate_terrain() -> World {
    let size = 500;

    let mut world = World::new("noisy_terrain".to_owned(), size as f64, size as f64);

    let noise = OpenSimplex::new().set_seed(random());

    let elevations = iproduct!(0..size, 0..size)
        .map(|(x, y)| {
            // noise is in range -1.0 to 1.0
            let elevation = (noise.get([x as f64 * 0.02, y as f64 * 0.02]) + 0.5) * 70.0;
            DataPoint::new(x as f64, y as f64, elevation)
        })
        .collect();

    world.elevation.insert(elevations);

    world
}