extern crate noise;
extern crate open_world_map;
extern crate clap;
extern crate itertools;
extern crate rand;

use std::io;

mod simple_noise;

fn main() -> Result<(), io::Error> {
    let noisy_world = simple_noise::generate_terrain();
    open_world_map::render::render_world(&noisy_world, "noisy_world.png")?;

    Ok(())
}
