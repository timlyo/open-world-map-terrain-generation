# Open World Map Terrain Generation

Collection of terrain generation algorithms that are compatible with Open World Map

Latest generated map:


![Map hasn't loaded, CI pipeline might be generating it now](https://gitlab.com/timlyo/open-world-map-terrain-generation/-/jobs/artifacts/master/raw/images/noisy_world.png?job=generate)